<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h2>Halaman form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="nama">Nama Lengkap</label><br>
        <input type="text" name="nama"><br><br>
        <label for="alamat">Alamat</label><br>
        <textarea name="alamat" id="alamat" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Kirim">
    </form>
</body>
</html>