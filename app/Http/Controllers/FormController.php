<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request) {
        $nama = $request['nama'];
        $alamat = $request['alamat'];
        return view('halaman.selamatDatang', compact('nama', 'alamat'));
        // dd($request->all());
    }
}
